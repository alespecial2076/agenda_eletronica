import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
class CpfGenerator {
  
  static Future<String> getCpfGenerator() async {
    
    var url = "https://www.4devs.com.br/ferramentas_online.php";
    
    print("GET > $url");


    var response = await http.post(
      Uri.parse('https://www.4devs.com.br/ferramentas_online.php'),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: <String, String>{
        'acao': 'gerar_cpf',
      },
    );

    //print("response ${response.body}");
    
    String text = response.body;
    text = text.replaceAll("<p>", "");
    text = text.replaceAll("</p>", "");

    return text;
  }
  
}