import 'package:agenda_eletronica/mobx/UsuarioModel.dart';
import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/telas/tela_informacoes.dart';
import 'package:agenda_eletronica/telas/tela_usuarios.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Metodos.dart';
import 'package:agenda_eletronica/widgets/circle/CircleAvatarCache.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';

class DrawerInicio extends StatefulWidget {

  DrawerInicio();
  @override
  _DrawerInicioState createState() => _DrawerInicioState();
}

class _DrawerInicioState extends State<DrawerInicio> {

  Widget _header() {
    return Observer(
      builder: (_){
        final UsuarioModel usuarioModel = GetIt.I<UsuarioModel>();
        User usuario = usuarioModel.usuario;
        if(usuario == null){
          return Container();
        }
        return GestureDetector(
          onTap: (){
            push(context, TelaUsuarios());
          },
          child: UserAccountsDrawerHeader(
            //accountName: TextApp(text: usuario.nome),
            accountName: Text("${usuario.nome}"),
            accountEmail: Text("${usuario.email}"),
            currentAccountPicture:
            usuario != null && usuario.urlFoto != null
                && usuario.urlFoto != "null"
                ?
            CircleAvatarCache(
              urlImagem: usuario.urlFoto,
              size: 64,
              //backgroundImage: NetworkImage(),
            )
                :
            CircleAvatar(
              radius: 40,
              child: Image.asset("assets/imagens/avatar.png", ),
              backgroundColor: Colors.transparent,
            ),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {

    final UsuarioModel usuarioModel = GetIt.I<UsuarioModel>();
    User usuario = usuarioModel.usuario;
    double larguraTela = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Observer(
        builder: (_){
          usuario = usuarioModel.usuario;

          return Container(
            width: larguraTela / 100 * 72,
            child: Drawer(
              child: ListView(
                children: [
                  usuario != null ?  _header() : _headerConvidado(),
                  ListTile(
                    leading: Icon(Icons.account_circle_sharp),
                    title: Text("Usuários"),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: (){
                      push(context, TelaUsuarios());
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.assignment_rounded),
                    title: Text("Contatos"),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: (){
                      popTela(context);
                    },
                  ),
                  ListTile(
                    leading: Icon(Icons.info),
                    title: Text("Informações"),
                    //subtitle: Text("mais informações..."),
                    //trailing: Icon(Icons.arrow_forward),
                    onTap: (){
                      popTela(context);
                      push(context, TelaInformacoes());
                    },
                  ),

                  SizedBox(height: 16,),
                  //banner logo
                  _bannerLogo(),


                ],
              ),
            ),
          );
        },
      ),
    );
  }

  _bannerLogo(){
    return  GestureDetector(
      onTap: (){
        popTela(context);
      },
      child: Container(
        margin: EdgeInsets.all(32),
        width: MediaQuery.of(context).size.width,
        //height: 84,
        child: Image.asset(
          "assets/imagens/logoSemFundo.png",
          //height: 64,
          fit: BoxFit.fill,
        ),
      ),
    );
  }

  _headerConvidado(){
    final UsuarioModel usuarioModel = GetIt.I<UsuarioModel>();
    User usuario = usuarioModel.usuario;

    return GestureDetector(
      onTap: (){
        push(context, TelaUsuarios());
      },
      child: Container(
        padding: EdgeInsets.all(16),
        color: Cores.COR_PRIMARY,
        //height: 128,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CircleAvatar(
              radius: 30,
              child: Image.asset("assets/imagens/avatar.png"),
              backgroundColor: Colors.transparent,
            ),
            SizedBox(height: 8,),
            TextApp(
              text: "Olá convidado,",
              textColor: Colors.white,
              fontSize: 14,
            ),
            SizedBox(height: 4,),
            TextApp(
              text: "Faça login para ter acesso completo",
              textColor: Colors.white,
            ),

          ],
        ),
      ),
    );
  }

}
