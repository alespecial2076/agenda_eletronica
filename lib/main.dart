import 'package:agenda_eletronica/mobx/ContatosModel.dart';
import 'package:agenda_eletronica/mobx/UsuarioModel.dart';
import 'package:agenda_eletronica/telas/tela_inicio.dart';
import 'package:agenda_eletronica/telas/tela_splash.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Strings.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

void main() {
  setUpLocators();
  runApp(MyApp());
}

void setUpLocators() {
  GetIt.I.registerSingleton(ContatosModel());
  GetIt.I.registerSingleton(UsuarioModel());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Strings.NOME_APP,
      theme: ThemeData(
          primarySwatch: Colors.purple,
          primaryColor: Cores.COR_PRIMARY,
          fontFamily: 'Roboto',
          backgroundColor: Cores.COR_PRIMARY
      ),
      home: TelaSplash(),
    );
  }
}

