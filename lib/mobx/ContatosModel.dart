import 'package:agenda_eletronica/models/contatos/Contato.dart';
import 'package:mobx/mobx.dart';

part 'ContatosModel.g.dart';

class ContatosModel = _ContatosModel with _$ContatosModel;

abstract class _ContatosModel with Store{

  @observable
  ObservableList<Contato> listContatos = ObservableList();

  @action
  void insertContato(int index, Contato value){
    listContatos.insert(index, value);
  }

  @action
  void addContato(Contato value) {
    listContatos.add(value);
  }

  @action
  void removeWhere(int idContato){
    listContatos.removeWhere((contato) => idContato == contato.id);
  }

  @action
  void limparLista(){
    listContatos.clear();
  }

  @action
  void attContato(int index, Contato value){
    listContatos.removeWhere((contato) => contato.id == value.id);
    listContatos.insert(index, value);
  }

  @action
  void sort(){
    listContatos.sort((b, a){
      //return a.dataFiltro.millisecondsSinceEpoch.compareTo(b.dataFiltro.millisecondsSinceEpoch);
      //DateTime d1 = DateTime.parse(a.dataFiltro.toDate().toString());
      //DateTime d2 = DateTime.parse(b.dataFiltro.toDate().toString());
     // return d1.compareTo(d2);
      return;
    });
  }

}