// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ContatosModel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ContatosModel on _ContatosModel, Store {
  final _$listContatosAtom = Atom(name: '_ContatosModel.listContatos');

  @override
  ObservableList<Contato> get listContatos {
    _$listContatosAtom.reportRead();
    return super.listContatos;
  }

  @override
  set listContatos(ObservableList<Contato> value) {
    _$listContatosAtom.reportWrite(value, super.listContatos, () {
      super.listContatos = value;
    });
  }

  final _$_ContatosModelActionController =
      ActionController(name: '_ContatosModel');

  @override
  void insertContato(int index, Contato value) {
    final _$actionInfo = _$_ContatosModelActionController.startAction(
        name: '_ContatosModel.insertContato');
    try {
      return super.insertContato(index, value);
    } finally {
      _$_ContatosModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addContato(Contato value) {
    final _$actionInfo = _$_ContatosModelActionController.startAction(
        name: '_ContatosModel.addContato');
    try {
      return super.addContato(value);
    } finally {
      _$_ContatosModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeWhere(int idContato) {
    final _$actionInfo = _$_ContatosModelActionController.startAction(
        name: '_ContatosModel.removeWhere');
    try {
      return super.removeWhere(idContato);
    } finally {
      _$_ContatosModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void limparLista() {
    final _$actionInfo = _$_ContatosModelActionController.startAction(
        name: '_ContatosModel.limparLista');
    try {
      return super.limparLista();
    } finally {
      _$_ContatosModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void attContato(int index, Contato value) {
    final _$actionInfo = _$_ContatosModelActionController.startAction(
        name: '_ContatosModel.attContato');
    try {
      return super.attContato(index, value);
    } finally {
      _$_ContatosModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void sort() {
    final _$actionInfo = _$_ContatosModelActionController.startAction(
        name: '_ContatosModel.sort');
    try {
      return super.sort();
    } finally {
      _$_ContatosModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
listContatos: ${listContatos}
    ''';
  }
}
