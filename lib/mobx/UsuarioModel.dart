import 'package:agenda_eletronica/models/users/User.dart';
import 'package:mobx/mobx.dart';

part 'UsuarioModel.g.dart';

class UsuarioModel = _UsuarioModel with _$UsuarioModel;

abstract class _UsuarioModel with Store{

  @observable
  User usuario;

  @observable
  ObservableList<User> listUsers = ObservableList();

  @action
  void setUser(User value){
    usuario = value;
  }

  @action
  void insertUser(int index, User value){
    listUsers.insert(index, value);
  }

  @action
  void addUser(User value) {
    listUsers.add(value);
  }

  @action
  void removeWhere(int idUser){
    listUsers.removeWhere((user) => idUser == user.id);
  }

  @action
  void limparLista(){
    listUsers.clear();
  }

  @action
  void attUser(int index, User value){
    listUsers.removeWhere((user) => user.id == value.id);
    listUsers.insert(index, value);
  }


}