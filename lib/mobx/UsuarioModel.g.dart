// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'UsuarioModel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UsuarioModel on _UsuarioModel, Store {
  final _$usuarioAtom = Atom(name: '_UsuarioModel.usuario');

  @override
  User get usuario {
    _$usuarioAtom.reportRead();
    return super.usuario;
  }

  @override
  set usuario(User value) {
    _$usuarioAtom.reportWrite(value, super.usuario, () {
      super.usuario = value;
    });
  }

  final _$listUsersAtom = Atom(name: '_UsuarioModel.listUsers');

  @override
  ObservableList<User> get listUsers {
    _$listUsersAtom.reportRead();
    return super.listUsers;
  }

  @override
  set listUsers(ObservableList<User> value) {
    _$listUsersAtom.reportWrite(value, super.listUsers, () {
      super.listUsers = value;
    });
  }

  final _$_UsuarioModelActionController =
      ActionController(name: '_UsuarioModel');

  @override
  void setUser(User value) {
    final _$actionInfo = _$_UsuarioModelActionController.startAction(
        name: '_UsuarioModel.setUser');
    try {
      return super.setUser(value);
    } finally {
      _$_UsuarioModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void insertUser(int index, User value) {
    final _$actionInfo = _$_UsuarioModelActionController.startAction(
        name: '_UsuarioModel.insertUser');
    try {
      return super.insertUser(index, value);
    } finally {
      _$_UsuarioModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addUser(User value) {
    final _$actionInfo = _$_UsuarioModelActionController.startAction(
        name: '_UsuarioModel.addUser');
    try {
      return super.addUser(value);
    } finally {
      _$_UsuarioModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void removeWhere(int idUser) {
    final _$actionInfo = _$_UsuarioModelActionController.startAction(
        name: '_UsuarioModel.removeWhere');
    try {
      return super.removeWhere(idUser);
    } finally {
      _$_UsuarioModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void limparLista() {
    final _$actionInfo = _$_UsuarioModelActionController.startAction(
        name: '_UsuarioModel.limparLista');
    try {
      return super.limparLista();
    } finally {
      _$_UsuarioModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  void attUser(int index, User value) {
    final _$actionInfo = _$_UsuarioModelActionController.startAction(
        name: '_UsuarioModel.attUser');
    try {
      return super.attUser(index, value);
    } finally {
      _$_UsuarioModelActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
usuario: ${usuario},
listUsers: ${listUsers}
    ''';
  }
}
