import 'dart:typed_data';

import 'package:agenda_eletronica/sql/entity.dart';
import 'dart:convert' as convert;

class Contato extends Entity{

  int _id;
  String _nome;
  String _sobreNome;
  String _cpf;
  String _email;
  String _urlFoto;
  String _telefoneResidencial;
  String _telefoneCelular;
  String _telefoneTrabalho;
  Uint8List _fotoUint8;

  Contato();

  Contato.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    nome = map['nome'];
    sobreNome = map['sobreNome'];
    email = map['email'];
    cpf = map['cpf'];
    urlFoto = map['urlFoto'];
    telefoneCelular = map['telefoneCelular'];
    telefoneResidencial = map['telefoneResidencial'];
    telefoneTrabalho = map['telefoneTrabalho'];
    fotoUint8 = map['fotoUint8'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nome'] = this.nome;
    data['sobreNome'] = this.sobreNome;
    data['email'] = this.email;
    data['cpf'] = this.cpf;
    data['urlFoto'] = this.urlFoto;
    data['telefoneCelular'] = this.telefoneCelular;
    data['telefoneResidencial'] = this.telefoneResidencial;
    data['telefoneTrabalho'] = this.telefoneTrabalho;
    data['fotoUint8'] = this.fotoUint8;
    return data;
  }

  Contato.fromJson(Map<String, dynamic> map){
    id = map["id"];
    nome = map["nome"];
    sobreNome = map["sobreNome"];
    cpf = map["cpf"];
    email = map["email"];
    urlFoto = map["urlFoto"];
    telefoneCelular = map["telefoneCelular"];
    telefoneResidencial = map["telefoneResidencial"];
    telefoneTrabalho = map["telefoneTrabalho"];
    fotoUint8 = map["fotoUint8"];
  }

  String toJson() {
    String json = convert.json.encode(toMap());
    return json;
  }

  Uint8List get fotoUint8 => _fotoUint8;

  set fotoUint8(Uint8List value) {
    _fotoUint8 = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get nome => _nome;

  set nome(String value) {
    _nome = value;
  }

  String get sobreNome => _sobreNome;

  String get telefoneTrabalho => _telefoneTrabalho;

  set telefoneTrabalho(String value) {
    _telefoneTrabalho = value;
  }

  String get telefoneCelular => _telefoneCelular;

  set telefoneCelular(String value) {
    _telefoneCelular = value;
  }

  String get telefoneResidencial => _telefoneResidencial;

  set telefoneResidencial(String value) {
    _telefoneResidencial = value;
  }

  String get urlFoto => _urlFoto;

  set urlFoto(String value) {
    _urlFoto = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get cpf => _cpf;

  set cpf(String value) {
    _cpf = value;
  }

  set sobreNome(String value) {
    _sobreNome = value;
  }

  @override
  String toString() {
    return 'Contato{_id: $_id, _nome: $_nome, _sobreNome: $_sobreNome, _cpf: $_cpf, _email: $_email, _urlFoto: $_urlFoto, _telefoneResidencial: $_telefoneResidencial, _telefoneCelular: $_telefoneCelular, _telefoneTrabalho: $_telefoneTrabalho}';
  }
}