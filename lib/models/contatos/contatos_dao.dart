
import 'package:agenda_eletronica/models/contatos/Contato.dart';
import 'package:agenda_eletronica/sql/base_dao.dart';
import 'package:agenda_eletronica/sql/db_helper.dart';
import 'package:sqflite/sqflite.dart';

// Data Access Object
class ContatoDAO extends BaseDAO<Contato>{

  Future<Database> get db => DatabaseHelper.getInstance().db;

  Future<int> save(Contato contato) async {
    var dbClient = await db;
    var id = await dbClient.insert("contatos", contato.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    //print('id: $id');
    return id;
  }

  Future<int> update(Contato contato) async {
    var dbClient = await db;
    int updateCount = await dbClient.rawUpdate('''
    UPDATE contatos 
    SET nome = ?, sobreNome = ?, email = ?, cpf = ?, urlFoto = ?, fotoUint8 = ?, telefoneCelular = ?, telefoneTrabalho = ?, telefoneResidencial = ?
    WHERE id = ?
    ''',
        [contato.nome, contato.sobreNome, contato.email, contato.cpf, contato.urlFoto, contato.fotoUint8, contato.telefoneCelular, contato.telefoneTrabalho, contato.telefoneResidencial, contato.id]);

    print("updateCount: $updateCount");
    return updateCount;

  }

  Future<List<Contato>> findAll() async {
    final dbClient = await db;

    final list = await dbClient.rawQuery('select * from contatos');

    final contatos = list.map<Contato>((json) => Contato.fromJson(json)).toList();

    return contatos;
  }

  Future<List<Contato>> findAllByTipo(String tipo) async {
    final dbClient = await db;

    final list = await dbClient.rawQuery('select * from contatos where tipo =? ',[tipo]);

    final contatos = list.map<Contato>((json) => Contato.fromJson(json)).toList();

    return contatos;
  }

  Future<Contato> findById(int id) async {
    var dbClient = await db;
    final list =
    await dbClient.rawQuery('select * from contatos where id = ?', [id]);

    if (list.length > 0) {
      return new Contato.fromJson(list.first);
    }

    return null;
  }

  Future<bool> existe(Contato contato) async {
    Contato u = await findById(contato.id);
    var exists = u != null;
    return exists;
  }

  Future<int> count() async {
    final dbClient = await db;
    final list = await dbClient.rawQuery('select count(*) from contatos');
    return Sqflite.firstIntValue(list);
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from contatos where id = ?', [id]);
  }

  Future<int> deleteAll() async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from contatos');
  }

  @override
  Contato fromMap(Map<String, dynamic> map) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  @override
  // TODO: implement tableName
  String get tableName => throw UnimplementedError();

}
