import 'dart:convert' as convert;
import 'dart:typed_data';

import 'package:agenda_eletronica/sql/entity.dart';

class User extends Entity{

  int _id;
  String _nome;
  String _nomeUser;
  String _email;
  String _telefone;
  String _site;
  String _urlFoto;
  Uint8List _fotoUint8;

  //Address
  String _street;
  String _suite;
  String _city;
  String _zipcode;
  String _lat;
  String _lng;

  //Company
  String _nameCompany;
  String _catchPhrase;
  String _bs;


  User.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    nome = map['name'];
    nomeUser = map['username'];
    email = map['email'];
    telefone = map['phone'];
    site = map['website'];
    fotoUint8 = map['fotoUint8'];

    //Address
    street = map['address']['street'];
    suite = map['address']['suite'];
    city = map['address']['city'];
    zipcode = map['address']['zipcode'];
    lat = map['address']["geo"]['lat'];
    lng = map['address']["geo"]['lng'];

    //Company
    nameCompany = map['company']['name'];
    catchPhrase = map['company']['catchPhrase'];
    bs = map['company']['bs'];
  }

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.nome;
    data['username'] = this.nomeUser;
    data['email'] = this.email;
    data['website'] = this.site;
    data['phone'] = this.telefone;
    data['fotoUint8'] = this.fotoUint8;
    return data;
  }

  User.fromJson(Map<String, dynamic> map){
    id = map["id"];
    nome = map["name"];
    nomeUser = map["username"];
    email = map["email"];
    telefone = map["phone"];
    site = map["website"];
    fotoUint8 = map["fotoUint8"];
  }

  String toJson() {
    String json = convert.json.encode(toMap());
    return json;
  }


  String get nameCompany => _nameCompany;

  set nameCompany(String value) {
    _nameCompany = value;
  }

  String get street => _street;

  set street(String value) {
    _street = value;
  }

  Uint8List get fotoUint8 => _fotoUint8;

  set fotoUint8(Uint8List value) {
    _fotoUint8 = value;
  }

  String get urlFoto => _urlFoto;

  set urlFoto(String value) {
    _urlFoto = value;
  }

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get nome => _nome;

  String get site => _site;

  set site(String value) {
    _site = value;
  }

  String get telefone => _telefone;

  set telefone(String value) {
    _telefone = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  String get nomeUser => _nomeUser;

  set nomeUser(String value) {
    _nomeUser = value;
  }

  set nome(String value) {
    _nome = value;
  }

  String get suite => _suite;

  set suite(String value) {
    _suite = value;
  }

  String get city => _city;

  set city(String value) {
    _city = value;
  }

  String get zipcode => _zipcode;

  set zipcode(String value) {
    _zipcode = value;
  }

  String get lat => _lat;

  set lat(String value) {
    _lat = value;
  }

  String get lng => _lng;

  set lng(String value) {
    _lng = value;
  }

  String get catchPhrase => _catchPhrase;

  set catchPhrase(String value) {
    _catchPhrase = value;
  }

  String get bs => _bs;

  set bs(String value) {
    _bs = value;
  }

  @override
  String toString() {
    return 'User{_id: $_id, _nome: $_nome, _nomeUser: $_nomeUser, _email: $_email, _telefone: $_telefone, _site: $_site, _urlFoto: $_urlFoto, _street: $_street, _suite: $_suite, _city: $_city, _zipcode: $_zipcode, _lat: $_lat, _long: $_lng, _nameCompany: $_nameCompany, _catchPhrase: $_catchPhrase, _bs: $_bs}';
  }
}