
import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/sql/base_dao.dart';
import 'package:agenda_eletronica/sql/db_helper.dart';
import 'package:sqflite/sqflite.dart';

// Data Access Object
class UserDAO extends BaseDAO<User>{

  Future<Database> get db => DatabaseHelper.getInstance().db;

  Future<int> save(User user) async {
    var dbClient = await db;
    var id = await dbClient.insert("user", user.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    //print('id: $id');
    return id;
  }

  Future<List<User>> findAll() async {
    final dbClient = await db;

    final list = await dbClient.rawQuery('select * from user');

    final users = list.map<User>((json) => User.fromJson(json)).toList();

    return users;
  }

  Future<List<User>> findAllByTipo(String tipo) async {
    final dbClient = await db;

    final list = await dbClient.rawQuery('select * from user where tipo =? ',[tipo]);

    final users = list.map<User>((json) => User.fromJson(json)).toList();

    return users;
  }

  Future<User> findById(int id) async {
    var dbClient = await db;
    final list =
    await dbClient.rawQuery('select * from user where id = ?', [id]);

    if (list.length > 0) {
      return new User.fromJson(list.first);
    }

    return null;
  }

  Future<bool> existe(User user) async {
    User u = await findById(user.id);
    var exists = u != null;
    return exists;
  }

  Future<int> count() async {
    final dbClient = await db;
    final list = await dbClient.rawQuery('select count(*) from user');
    return Sqflite.firstIntValue(list);
  }

  Future<int> delete(int id) async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from user where id = ?', [id]);
  }

  Future<int> deleteAll() async {
    var dbClient = await db;
    return await dbClient.rawDelete('delete from user');
  }

  @override
  User fromMap(Map<String, dynamic> map) {
    // TODO: implement fromMap
    throw UnimplementedError();
  }

  @override
  // TODO: implement tableName
  String get tableName => throw UnimplementedError();

}
