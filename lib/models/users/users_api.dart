
import 'dart:convert' as convert;
import 'dart:io';

import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/models/users/user_dao.dart';
import 'package:agenda_eletronica/utils/http_helper.dart' as http;

class UsersApi {

  static Future<List<User>> getUsers() async{
    var url = 'https://jsonplaceholder.typicode.com/users';

    print("GET > $url");

    var response = await http.get(url);

    String json = response.body;
    //print("json >> $json");

    List list = convert.json.decode(json);

    final List<User> users = list.map<User>( (map) => User.fromMap(map) ).toList();

    final userDao = UserDAO();

    //Salvar todos os usuários no bd
    for(User user in users){
      userDao.save(user);
    }

    return users;

  }



}