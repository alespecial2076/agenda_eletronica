import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:agenda_eletronica/api/cpf_generator.dart';
import 'package:agenda_eletronica/mobx/ContatosModel.dart';
import 'package:agenda_eletronica/models/contatos/Contato.dart';
import 'package:agenda_eletronica/models/contatos/contatos_dao.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Metodos.dart';
import 'package:agenda_eletronica/utils/alertas.dart';
import 'package:agenda_eletronica/widgets/botoes/BotaoPrimary.dart';
import 'package:agenda_eletronica/widgets/outros/ImageSourceSheet.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:agenda_eletronica/widgets/texts/text_form_field_app.dart';
import 'package:brasil_fields/formatter/telefone_input_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:validadores/Validador.dart';


class ModalContato extends StatefulWidget {

  Contato contato;
  int index;
  ModalContato({this.contato, this.index});

  @override
  _ModalContatoState createState() => _ModalContatoState();
}

class _ModalContatoState extends State<ModalContato> {

  Contato get contato => widget.contato;
  int get index => widget.index;

  final ContatosModel contatosModel = GetIt.I<ContatosModel>();

  final _formKey = GlobalKey<FormState>();
  final TextEditingController _controllerNome = TextEditingController();
  final TextEditingController _controllerSobreNome = TextEditingController();
  final TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerTelefone = TextEditingController();
  TextEditingController _controllerTelefoneTrabalho = TextEditingController();
  TextEditingController _controllerTelefoneResidencial = TextEditingController();
  TextEditingController _controllerCpf = TextEditingController();

  final maskCNPJ = MaskTextInputFormatter(mask: "##.###.###/####-##", filter: {"#": RegExp(r'[0-9]')});
  final maskCpf = MaskTextInputFormatter(mask: "###.###.###-##", filter: {"#": RegExp(r'[0-9]')});

  File _imagemContato;

  bool _carregando = false;
  bool _editar = false;

  @override
  void initState() {
    super.initState();

    if(contato != null){
      _editar = true;
      _controllerNome.text = contato.nome ?? "";
      _controllerSobreNome.text = contato.sobreNome ?? "";
      _controllerTelefone.text = contato.telefoneCelular ?? "";
      _controllerCpf.text = contato.cpf ?? "";
      _controllerEmail.text = contato.email ?? "";
      _controllerTelefoneTrabalho.text = contato.telefoneTrabalho ?? "";
      _controllerTelefoneResidencial.text = contato.telefoneResidencial ?? "";
    }else{
      widget.contato = Contato();
    }
  }

  @override
  Widget build(BuildContext context) {

    double larguraTela = MediaQuery.of(context).size.width;
    double alturaTela = MediaQuery.of(context).size.height;

    return Material(
        child: CupertinoPageScaffold(
          child: SafeArea(
              bottom: false,
              child: Container(
                width: double.infinity,
                height: double.infinity,
                color: Cores.COR_CINZA_BACKGROUND,
                child:Stack(
                  children: [
                    SingleChildScrollView(
                        controller: Platform.isIOS
                            ?  ModalScrollController.of(context)
                            : null,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                padding: EdgeInsets.all(8),
                                //height: 120,
                                width: double.infinity,
                                //color: Cores.COR_PRIMARY_OPACITY,
                                color: Cores.COR_PRIMARY,
                                child: Column(
                                  children: [
                                    GestureDetector(
                                      onTap: (){
                                        showModalBottomSheet(
                                            context: context,
                                            builder: (context) => ImageSourceSheet(
                                              onImagemSelecionada: (imagemSelecionada){
                                                Navigator.of(context).pop();
                                                setState(() {
                                                  _imagemContato = imagemSelecionada;
                                                });
                                              },
                                            )
                                        );
                                      },
                                      child:
                                      _editar && _imagemContato == null && contato.fotoUint8 != null
                                          ? CircleAvatar(
                                        backgroundImage:  MemoryImage(contato.fotoUint8),
                                        backgroundColor: Colors.blueGrey[50],
                                        radius: 40,
                                      ) :
                                      _imagemContato == null ?
                                      Column(
                                        children: [
                                          CircleAvatar(
                                            radius: 40,
                                            child: Image.asset("assets/imagens/avatar.png"),
                                            backgroundColor: Colors.transparent,
                                          ),
                                        ],
                                      )
                                          : CircleAvatar(
                                        radius: 36,
                                        child: ClipOval(
                                          child: Image.file(_imagemContato),
                                        ),
                                        backgroundColor: Colors.transparent,
                                      ),
                                    ),
                                    SizedBox(height: 4,),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        !_editar ?
                                        TextApp(
                                          text: "Trocar foto",
                                          textColor: Colors.white,
                                          fontSize: 12,
                                        ) : Container(),
                                        _editar ?
                                        TextApp(
                                          text: "${contato.nome} ${contato.sobreNome ?? ""}",
                                          textColor: Colors.white,
                                          bold: true,
                                          fontSize: 18,
                                        ) : Container(),
                                      ],
                                    ),
                                    //PARTE COMUNICAÇÃO
                                    _editar ? _parteComunicacao() : Container(),
                                  ],
                                )
                            ),
                            //parte formulario
                            Container(
                              padding: EdgeInsets.all(16),
                              child: Form(
                                key: _formKey,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: TextApp(
                                            text: "Nome",
                                            textColor: Cores.COR_DARK_LIGTH,
                                            fontSize: 16,
                                          ),
                                        ),
                                        _editar ?
                                        GestureDetector(
                                          onTap: () async{
                                            return alertaDelete(
                                              context,
                                              titulo: "Excluir contato",
                                              msg: "Deseja realmente excluir o contato de ${contato.nome}?",
                                              funcExcluir: () async{
                                                ContatoDAO contatoDao = ContatoDAO();
                                                contatoDao.delete(contato.id);
                                                await _getContatos();
                                                popTela(context);
                                              }
                                            );
                                          },
                                          child: TextApp(
                                            text: "EXCLUIR",
                                            textColor: Cores.COR_VERMELHA,
                                            fontSize: 14,
                                          )
                                        ): Container(),
                                        !_editar ?
                                        GestureDetector(
                                          onTap: () async{
                                            String numberCpf = await CpfGenerator.getCpfGenerator();
                                            String cpfFormatado = maskCpf.maskText(numberCpf);
                                            setState(() {
                                              _controllerNome.text = "Alex";
                                              _controllerSobreNome.text = "Anderson";
                                              _controllerTelefone.text = "(31) 98906-4421";
                                              _controllerEmail.text = "email@gmail.com";
                                              _controllerCpf.text = cpfFormatado;
                                            });
                                          },
                                          child: TextApp(
                                            text: "GERAR DADOS",
                                            textColor: Cores.COR_PRIMARY,
                                            fontSize: 14,
                                          ),
                                        ) : Container(),
                                      ],
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      hintText: "Nome do contato",
                                      obscureText: false,
                                      controller: _controllerNome,
                                      validator: _validarNome,
                                      keyboardType: TextInputType.text,
                                    ),
                                    SizedBox(height: 16,),
                                    Row(
                                      children: [
                                        TextApp(
                                          text: "Sobrenome",
                                          textColor: Cores.COR_DARK_LIGTH,
                                          fontSize: 16,
                                        ),
                                        SizedBox(width: 2,),
                                        TextApp(
                                          text: "(opcional)",
                                          textColor: Colors.grey[400],
                                          fontSize: 14,
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      hintText: "Sobrenome do contato",
                                      obscureText: false,
                                      controller: _controllerSobreNome,
                                      //validator: _validarSobreNome,
                                      keyboardType: TextInputType.text,
                                    ),
                                    SizedBox(height: 16,),
                                    TextApp(
                                      text: "Celular (com DDD)",
                                      textColor: Cores.COR_DARK_LIGTH,
                                      fontSize: 16,
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      hintText: "(00) 90000-0000",
                                      obscureText: false,
                                      controller: _controllerTelefone,
                                      keyboardType: TextInputType.number,
                                      validator: _validarTelefone,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        TelefoneInputFormatter()
                                      ],
                                    ),
                                    SizedBox(height: 16,),
                                    Row(
                                      children: [
                                        TextApp(
                                          text: "Tel trabalho",
                                          textColor: Cores.COR_DARK_LIGTH,
                                          fontSize: 16,
                                        ),
                                        SizedBox(width: 2,),
                                        TextApp(
                                          text: "(opcional)",
                                          textColor: Colors.grey[400],
                                          fontSize: 14,
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      hintText: "(00) 90000-0000",
                                      obscureText: false,
                                      controller: _controllerTelefoneTrabalho,
                                      keyboardType: TextInputType.number,
                                      //validator: _validarTelefone,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        TelefoneInputFormatter()
                                      ],
                                    ),
                                    SizedBox(height: 16,),
                                    Row(
                                      children: [
                                        TextApp(
                                          text: "Tel residencial",
                                          textColor: Cores.COR_DARK_LIGTH,
                                          fontSize: 16,
                                        ),
                                        SizedBox(width: 2,),
                                        TextApp(
                                          text: "(opcional)",
                                          textColor: Colors.grey[400],
                                          fontSize: 14,
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      hintText: "(00) 90000-0000",
                                      obscureText: false,
                                      controller: _controllerTelefoneResidencial,
                                      keyboardType: TextInputType.number,
                                      //validator: _validarTelefone,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter.digitsOnly,
                                        TelefoneInputFormatter()
                                      ],
                                    ),
                                    SizedBox(height: 16,),
                                    txtInformativo("Dados pessoais"),
                                    SizedBox(height: 16,),
                                    Row(
                                      children: [
                                        TextApp(
                                          text: "E-mail",
                                          textColor: Cores.COR_DARK_LIGTH,
                                          fontSize: 16,
                                        ),
                                        SizedBox(width: 2,),
                                        TextApp(
                                          text: "(opcional)",
                                          textColor: Colors.grey[400],
                                          fontSize: 14,
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      hintText: "example@gmail.com",
                                      obscureText: false,
                                      controller: _controllerEmail,
                                      validator: _validarEmail,
                                      keyboardType: TextInputType.emailAddress,
                                    ),
                                    SizedBox(height: 16,),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        TextApp(
                                          text: "CPF",
                                          textColor: Cores.COR_DARK_LIGTH,
                                          fontSize: 16,
                                        ),
                                        GestureDetector(
                                          onTap: () async{
                                            String numberCpf = await CpfGenerator.getCpfGenerator();
                                            String cpfFormatado = maskCpf.maskText(numberCpf);
                                            setState(() {
                                              _controllerCpf.text = cpfFormatado;
                                            });
                                          },
                                          child: TextApp(
                                            text: "GERAR CPF",
                                            textColor: Cores.COR_PRIMARY,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(height: 8,),
                                    TextFormFieldApp(
                                      labelText: "CPF",
                                      hintText: "Digite seu cpf",
                                      obscureText: false,
                                      controller: _controllerCpf,
                                      //validator: _validarCpf,
                                      validator: (value) {
                                        // Aqui entram as validações
                                        return Validador()
                                            .add(Validar.CPF, msg: 'CPF Inválido')
                                            .add(Validar.OBRIGATORIO, msg: 'Campo obrigatório')
                                            .valido(value,clearNoNumber: true);
                                      },
                                      inputFormatters: [
                                        //FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                                        maskCpf
                                      ],
                                      //keyboardType: TextInputType.number,
                                    ),
                                    SizedBox(height: 48,),
                                  ],
                                ),
                              ),
                            ),

                          ],
                        )
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        child: BotaoPrimary(
                          carregando: _carregando,
                          text: "Salvar",
                          onPressed: () async{
                            bool formOk = _formKey.currentState.validate();

                            if(! formOk) {
                              return;
                            }

                            setState(() {
                              _carregando = true;
                            });



                            contato.nome = _controllerNome.text;
                            contato.email = _controllerEmail.text;
                            //contato.urlFoto = _imagemProduto;
                            contato.cpf = _controllerCpf.text;
                            contato.sobreNome = _controllerSobreNome.text;
                            contato.telefoneCelular = _controllerTelefone.text;
                            contato.telefoneTrabalho = _controllerTelefoneTrabalho.text;
                            contato.telefoneResidencial = _controllerTelefoneResidencial.text;


                            if(_imagemContato != null){
                              //salvar imagem
                              //Get the file in UInt8List format
                              Uint8List imageInBytes = _imagemContato.readAsBytesSync();
                              contato.fotoUint8 = imageInBytes;
                            }else{
                              //nao salvar img
                            }


                            if(!_editar){
                              final contatoDao = ContatoDAO();
                              await contatoDao.save(contato);
                              //contatosModel.addContato(contato);
                            }else{
                              final contatoDao = ContatoDAO();
                              await contatoDao.update(contato);
                              //contatosModel.removeWhere(index);
                              //contatosModel.insertContato(index, contato);
                            }
                            await _getContatos();

                            setState(() {
                              _carregando = false;
                            });

                            popTela(context);
                          },
                        ),
                      ),
                    )
                  ],
                )
              )
          ),
        ));
  }

  _parteComunicacao(){
    return Container(
      margin: EdgeInsets.only(top: 8),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            GestureDetector(
              onTap: () async{
                //popTela(context);
                _sendMessage();
              },
              child: _widgetComunicacao("mensagem", Icons.message),
            ),
            GestureDetector(
              onTap: () async{
                //popTela(context);
                await launch("tel://${contato.telefoneCelular}");
              },
              child: _widgetComunicacao("ligar", Icons.call),
            ),
            GestureDetector(
              onTap: () async{
                String numberFormatado =  contato.telefoneCelular.replaceAll(new RegExp(r'[^0-9]'),''); // '23'
                var url = "https://api.whatsapp.com/send?phone=55$numberFormatado.";
                _launchURL(url);
              },
              child: _widgetComunicacao("whatsapp", Icons.send),
            )
          ],
        ),
      ),
    );
  }

  _launchURL(url) async => await canLaunch(url)
      ? await launch(url) : throw 'Not found $url';

  _sendMessage() async {
    String url;
    String msg = "";
    if(Platform.isAndroid){
      //FOR Android
      url ='sms:+55${contato.telefoneCelular}?body=$msg';
      await launch(url);
    }
    else if(Platform.isIOS){
      //FOR IOS
      url ='sms:+55${contato.telefoneCelular}?body=$msg';
    }
  }

  _widgetComunicacao(String title, IconData icon){
    return  Container(
      width: 80,
      child: Card(
        color: Colors.grey[200],
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
        child: Container(
          padding: EdgeInsets.all(4),
          child: Column(
            children: [
              Icon(icon, color: Cores.COR_PRIMARY_DARK, size: 20,),
              TextApp(
                text: "$title",
                textColor: Cores.COR_PRIMARY_DARK,
                fontFamily: "OpenSans",
                fontSize: 10,
              )
            ],
          ),
        ),
      ),
    );
  }

  txtInformativo(String titulo){
    return Center(
      child: Container(
        width: double.infinity,
        //color: Cores.COR_PRIMARY,
        color: Cores.COR_PRIMARY_OPACITY3,
        padding: EdgeInsets.all(2),
        child: Center(
          child: TextApp(
            text: titulo,
            textColor: Cores.COR_DARK_LIGTH,
            //textColor: Colors.white,
            fontSize: 14,
            bold: true,
            fontFamily: "OpenSans",
          ),
        ),
      ),
    );
  }

  String _validarTelefone(String text) {
    if (text.trim().isEmpty) {
      return "Digite o telefone";
    }else if(text.length < 14){
      return "Digite um número válido";
    }
    return null;
  }

  String _validarNome(String text) {
    if (text.trim().isEmpty || text.trim().length == 0) {
      return "Digite o nome!";
    }else if(text.trim().length < 5){
      //return "Nome muito pequeno!";
    }
    return null;
  }

  String _validarSobreNome(String text) {
    if (text.trim().isEmpty || text.trim().length == 0) {
      return "Digite o sobrenome!";
    }else if(text.trim().length < 5){
     // return "Nome muito pequeno!";
    }
    return null;
  }

  String _validarEmail(String email) {
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);

    if (email.trim().isEmpty) {
      //return "Digite um e-mail!";
      return null;
    }else if(!emailValid){
      return "Digite um e-mail válido!";
    }
    return null;
  }

  _getContatos() async{
    contatosModel.limparLista();
    final contatoDao = ContatoDAO();
    List<Contato> listContatos = await contatoDao.findAll();

    for(Contato contato in listContatos){
      contatosModel.addContato(contato);
    }
  }

}
