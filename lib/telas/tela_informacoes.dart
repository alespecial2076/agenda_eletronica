import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Metodos.dart';
import 'package:agenda_eletronica/utils/Strings.dart';
import 'package:agenda_eletronica/widgets/botoes/BotaoPrimary.dart';
import 'package:agenda_eletronica/widgets/circle/CircleAvatarCache.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class TelaInformacoes extends StatefulWidget {
  @override
  _TelaInformacoesState createState() => _TelaInformacoesState();
}

class _TelaInformacoesState extends State<TelaInformacoes> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextApp(
          text: "Informações",
          textColor: Colors.white,
          fontSize: 16,
          fontFamily: "OpenSans",
        ),
      ),
      body: _body(),
    );
  }

  _body(){
    double tamanhoTela = MediaQuery.of(context).size.height;
    return SingleChildScrollView(
      child: Container(
        height: tamanhoTela,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatarCache(
              urlImagem: "https://i.imgur.com/XAgiaxX.jpeg",
              size: 144,
            ),
            SizedBox(height: 16,),
            Center(
              child: TextApp(
                text: "Aplicativo desenvolvido por ${Strings.NOME_DEV}",
                textColor: Cores.COR_DARK_LIGTH,
                fontSize: 16,
                fontFamily: "OpenSans",
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                GestureDetector(
                  onTap: () async{
                    const url = "${Strings.URL_LINKEDIN}";
                    if (await canLaunch(url))
                    await launch(url);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 8),
                    child: Image.asset("assets/imagens/iconLinkedin.png", height: 40,),
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    var url = "${Strings.URL_WPP}";
                    _launchURL(url);
                  },
                  child: Container(
                    margin: EdgeInsets.only(right: 8),
                    child: Image.asset("assets/imagens/iconWpp.png", height: 64,),
                  ),
                ),
              ],
            ),
            SizedBox(height: 16,),
            Container(
                margin: EdgeInsets.only(left: 32, right: 32),
                child: BotaoPrimary(text: "Voltar", onPressed: (){popTela(context);})),
            SizedBox(height: 72,),
          ],
        ),
      ),
    );
  }

  _launchURL(url) async => await canLaunch(url)
      ? await launch(url) : throw 'Not found $url';
  //
}
