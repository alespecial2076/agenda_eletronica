
import 'package:agenda_eletronica/drawer/DrawerInicio.dart';
import 'package:agenda_eletronica/mobx/ContatosModel.dart';
import 'package:agenda_eletronica/mobx/UsuarioModel.dart';
import 'package:agenda_eletronica/models/contatos/Contato.dart';
import 'package:agenda_eletronica/models/contatos/contatos_dao.dart';
import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/models/users/user_dao.dart';
import 'package:agenda_eletronica/models/users/users_api.dart';
import 'package:agenda_eletronica/telas/modal/modal_add_contato.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Metodos.dart';
import 'package:agenda_eletronica/utils/alertas.dart';
import 'package:agenda_eletronica/widgets/botoes/BotaoPrimary.dart';
import 'package:agenda_eletronica/widgets/cards/card_contato.dart';
import 'package:agenda_eletronica/widgets/circle/CircleAvatarCache.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:carousel_pro/carousel_pro.dart';

class TelaInicio extends StatefulWidget {
  @override
  _TelaInicioState createState() => _TelaInicioState();
}

class _TelaInicioState extends State<TelaInicio> {

  final UsuarioModel usuarioModel = GetIt.I<UsuarioModel>();
  final ContatosModel contatosModel = GetIt.I<ContatosModel>();
  List<Widget> imagens;

  bool _viuAlerta = false;


  List<String> itensMenu = ["Novo contato", "Deletar contatos"];
  _escolhaMenuItem(String itemEscolhido){

    switch(itemEscolhido){
      case "Novo contato" :
        openModal(context: context, tela: ModalContato());
        break;
      case "Deletar contatos" :
        if(contatosModel.listContatos.length <= 0){
          return alertaSimples(
            context,
            titulo: "Ops",
            msg: "Você não tem contato adicionado!"
          );
        }else{
          return alertaDelete(
              context,
              titulo: "Excluir todos contatos",
              msg: "Deseja realmente excluir todos os contatos salvos?",
              funcExcluir: () async{
                ContatoDAO contatoDao = ContatoDAO();
                await contatoDao.deleteAll();
                await _getContatos();
              }
          );
        }
        break;
    }

  }

  _getContatos() async{
    contatosModel.limparLista();
    final contatoDao = ContatoDAO();
    List<Contato> listContatos = await contatoDao.findAll();

    for(Contato contato in listContatos){
      contatosModel.addContato(contato);
    }
  }

  List<Widget> _recuperarListaCache() {
    List<String> fotos = [
      "https://i.imgur.com/QR6kbyv.png",
    ];
    List<String> listaUrlImagens = fotos;
    return listaUrlImagens.map((url) {
      var index = listaUrlImagens.indexOf(url);
      //print("url banner - $url");
      return GestureDetector(
        onTap: (){
          print(index + 1);
        },
        child: Container(
          height: 328,
          //width: 120,
          //margin: EdgeInsets.only(left: 16, right: 16),
          decoration: BoxDecoration(
              //borderRadius: BorderRadius.circular(8), //Borda imagem
              image: DecorationImage(
                  image: CachedNetworkImageProvider(url),
                  fit: BoxFit.fill
              )
          ),
        ),
      );
    }).toList();
  }


  @override
  void initState() {
    super.initState();

    //Recuperar imagens
    imagens = _recuperarListaCache();

    //Verificar se tem contato
    _mostrarAlerta();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextApp(
          text: "Inicio",
          textColor: Colors.white,
          fontSize: 16,
          fontFamily: "OpenSans",
        ),
        actions: [

          PopupMenuButton<String>(
            onSelected: _escolhaMenuItem,
            itemBuilder: (context){
              return itensMenu.map((String item) {
                return PopupMenuItem(
                  value: item,
                  child: Text(item),
                );
              }).toList();
            },
          ),

          /*
          IconButton(icon: Icon(Icons.add),
              onPressed: (){
                openModal(context: context, tela: ModalContato());
              }
            ),
           */
        ],
      ),
      body: _body(),
      drawer: DrawerInicio(),
      floatingActionButton: _floating(),
    );
  }

  _body(){
    return SingleChildScrollView(
      child: Column(
        children: [
          _widgetTopo(),
          //_widgetUser(),
          _listaContatos(),
          SizedBox(height: 72,),
        ],
      ),
    );
  }

  _mostrarAlerta() async{
    await Future.delayed(Duration(seconds: 3));
    if(_viuAlerta || contatosModel.listContatos != null && contatosModel.listContatos.length > 0){
      return;
    }
    setState(() {
      _viuAlerta = true;
    });
    return _dialogAddContato(context);
  }

  _listaContatos(){

    return Observer(
      builder: (_){

        List<Contato> listaContatos = contatosModel.listContatos;

        if(listaContatos == null || listaContatos.length == 0){
          return Container(
            margin: EdgeInsets.only(top: 8),
            child: Column(
              children: [
                TextApp(
                  text: "Nenhum contato adicionado",
                  fontSize: 14,
                  textColor: Cores.COR_DARK_LIGTH,
                ),
                SizedBox(height: 8,),
                Container(
                  margin: EdgeInsets.only(left: 16, right: 16),
                  child: BotaoPrimary(
                      text: "Adicionar Contato",
                      onPressed: (){
                        openModal(context: context, tela: ModalContato());
                      }
                  ),
                )

              ],
            ),
          );
        }

        return  Container(
          width: double.infinity,
          child: ListView.builder(
            //reverse: true,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: listaContatos.length,
            itemBuilder: (context, indice){

              Contato contato = listaContatos[indice];

              return GestureDetector(
                onTap: (){
                  //print("contato: $contato");
                  openModal(context: context, tela: ModalContato(contato: contato, index: indice,));
                },
                onLongPress: (){
                  return alertaDelete(
                      context,
                      titulo: "Excluir contato",
                      msg: "Deseja realmente excluir o contato de ${contato.nome}?",
                      funcExcluir: () async{
                        ContatoDAO contatoDao = ContatoDAO();
                        await contatoDao.delete(contato.id);
                        await _getContatos();
                        popTela(context);
                      }
                  );
                },
                child: CardContato(contato: contato,),
              );

            },
          ),
        );
      },
    );
  }

  _widgetTopo(){
    return Container(
      margin: EdgeInsets.only(top: 0),
      child: SizedBox(
        height: 186,
        child: Carousel(
          images: imagens,
          //tamanho do pontinho
          dotSize: 4.0,
          //Cor de background dos pontos
          dotBgColor: Colors.transparent,
          //Cor dos pontos
          dotColor: Colors.black45,
          // cor do ponto selecionado
          dotIncreasedColor: Colors.white,
          animationCurve: Curves.linear,
          animationDuration: Duration(seconds: 2),
          autoplay: true,
          //autoplayDuration: Duration(seconds: 10),
          autoplayDuration: Duration(seconds: 30),
          dotSpacing: 15.0,
          indicatorBgPadding: 5.0,
          borderRadius: true,
          //radius: Radius.circular(180.0),
          moveIndicatorFromBottom: 180.0,

          noRadiusForIndicator: true,
        ),
      ),
    );
  }

  _widgetUser(){
    return Container(
      padding: EdgeInsets.all(8),
      child: Row(
        children: [
          CircleAvatarCache(
            urlImagem: "https://www.infoescola.com/wp-content/uploads/2017/04/leao-126767138.jpg",
            size: 48,
          ),
          SizedBox(width: 8,),
          TextApp(
            text: "Nome usuário",
            textColor: Cores.COR_DARK_LIGTH,
          )
        ],
      ),
    );
  }

  _floating(){
    return FloatingActionButton(
      child: Icon(Icons.add),
      backgroundColor: Cores.COR_PRIMARY_LIGTH,
      onPressed: (){
        openModal(context: context, tela: ModalContato());
      },
    );
  }

  _dialogAddContato(BuildContext context) async{
    //dialog add contato
    await showDialog(context: context,
        //barrierDismissible: false, //desativar click fora da dialog
        builder: (context) {
          return WillPopScope(
            //onWillPop: () async => false, // não permitir clicar na opção de voltar
            child: AlertDialog(
              //title: Text("Cadastre um cupom"),
              content: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: (){
                        popTela(context);
                        openModal(context: context, tela: ModalContato());
                      },
                      child: Image.asset("assets/imagens/addContatos.png"),
                    ),
                    SizedBox(height: 8,),
                    TextApp(
                      text: "ADICIONE CONTATOS",
                      fontSize: 14,
                      fontFamily: "OpenSans",
                      textColor: Cores.COR_DARK,
                    ),
                    SizedBox(height: 4,),
                    TextApp(
                      text: "Adicione amigos e familiares em seus contatos e tenha controle de toda sua agenda com apenas alguns clicks.",
                      fontSize: 12,
                      fontFamily: "OpenSans",
                      textColor: Colors.grey,
                    ),
                    SizedBox(height: 24,),
                    BotaoPrimary(
                      text: "Adicionar",
                      onPressed: (){
                        popTela(context);
                        openModal(context: context, tela: ModalContato());
                      },
                    ),
                    SizedBox(height: 16,),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
