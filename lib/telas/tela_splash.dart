import 'package:agenda_eletronica/mobx/ContatosModel.dart';
import 'package:agenda_eletronica/mobx/UsuarioModel.dart';
import 'package:agenda_eletronica/models/contatos/Contato.dart';
import 'package:agenda_eletronica/models/contatos/contatos_dao.dart';
import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/models/users/user_dao.dart';
import 'package:agenda_eletronica/models/users/users_api.dart';
import 'package:agenda_eletronica/telas/tela_inicio.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Metodos.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

class TelaSplash extends StatefulWidget {
  @override
  _TelaSplashState createState() => _TelaSplashState();
}

class _TelaSplashState extends State<TelaSplash> {

  final UsuarioModel usuarioModel = GetIt.I<UsuarioModel>();
  final ContatosModel contatosModel = GetIt.I<ContatosModel>();

  _init() async{
    //Recuperar usuários api
    await _getUsersApi();

    //Recuperar usuários salvos
    await _getUsuarios();

    //Recuperar contatos salvos
    await _getContatos();

    await Future.delayed(Duration(seconds: 1));

    pushReplacement(context, TelaInicio());
  }

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _body()
    );
  }

  _body () {
    return Container(
      color: Cores.COR_PRIMARY,
      child: Center(
        child: Container(
          child:
          Image.asset('assets/imagens/logo.png', height: 144, width: 144,),
        ),
      ),
    );
  }


  _getContatos() async{
    contatosModel.limparLista();
    final contatoDao = ContatoDAO();
    List<Contato> listContatos = await contatoDao.findAll();

    for(Contato contato in listContatos){
      contatosModel.addContato(contato);
    }
  }

  _getUsuarios() async{
    usuarioModel.limparLista();
    final userDao = UserDAO();
    List<User> listUsers = await userDao.findAll();

    for(User user in listUsers){
      usuarioModel.addUser(user);
    }
  }

  _getUsersApi() async{
    UserDAO userDAO = UserDAO();
    int qtdUsers = await userDAO.count();
    if(qtdUsers <= 0){
      await UsersApi.getUsers();
    }
  }
}
