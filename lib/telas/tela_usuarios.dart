import 'package:agenda_eletronica/mobx/UsuarioModel.dart';
import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/utils/Metodos.dart';
import 'package:agenda_eletronica/widgets/cards/card_usuario.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get_it/get_it.dart';

class TelaUsuarios extends StatefulWidget {
  @override
  _TelaUsuariosState createState() => _TelaUsuariosState();
}

class _TelaUsuariosState extends State<TelaUsuarios> {

  final UsuarioModel usuarioModel = GetIt.I<UsuarioModel>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextApp(
          text: "Usuários",
          textColor: Colors.white,
          fontSize: 16,
          fontFamily: "OpenSans",
        ),
      ),
      body: _body(),
    );
  }

  _body(){
    return SingleChildScrollView(
      child: Column(
        children: [
          _listaUsuarios(),
          SizedBox(height: 72,),
        ],
      ),
    );
  }

  _listaUsuarios(){

    return Observer(
      builder: (_){
        List<User> listaUsuarios = usuarioModel.listUsers;

        if(listaUsuarios == null || listaUsuarios.length == 0){
          return Container(
            margin: EdgeInsets.only(top: 8),
            child: Column(
              children: [
                Center(
                  child: TextApp(
                    text: "Nenhum usuário encontrado",
                    fontSize: 14,
                    textColor: Cores.COR_DARK_LIGTH,
                  ),
                ),
              ],
            ),
          );
        }

        return  Container(
          width: double.infinity,
          child: ListView.builder(
            //reverse: true,
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemCount: listaUsuarios.length,
            itemBuilder: (context, indice){

              User user = listaUsuarios[indice];

              return GestureDetector(
                onTap: () async{
                  usuarioModel.setUser(user);
                  Fluttertoast.showToast(
                      msg: "Usuário setado com sucesso!",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      backgroundColor: Cores.COR_VERDE
                    //timeInSecForIos: 1
                  );
                  await Future.delayed(Duration(seconds: 1));
                  popTela(context);
                },
                child: CardUsuario(usuario: user,),
              );

            },
          ),
        );
      },
    );
  }
}
