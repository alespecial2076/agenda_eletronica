import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Cores {
  
  //static const Color COR_PRIMARY = Color();
  //static const int COR_PRIMARY = 0x9B4DEE;
  //static const Color COR_PRIMARY = Color(0x9B4DEE00);

  static const Color COR_PRIMARY = Color.fromRGBO(155, 77, 238, 1);
  static const Color COR_PRIMARY_DARK = Color.fromRGBO(124, 54, 199, 1);
  static const Color COR_PRIMARY_LIGTH = Color.fromRGBO(173, 112, 238, 1);
  static const Color COR_SECONDARY = Color.fromRGBO(241, 167, 74, 1);
  static Color COR_CINZA_BACKGROUND = Colors.grey[100];
  static const Color COR_DARK = Color.fromRGBO(72, 72, 72, 1);
  //static const Color COR_DARK_LIGTH = Color.fromRGBO(111, 108, 115, 1);
  static const Color COR_DARK_LIGTH = Color.fromRGBO(99, 96, 102, 1);
  static const Color COR_GREY = Color.fromRGBO(184, 179, 190, 1);
  static const Color COR_SUCESS = Color.fromRGBO(49, 174, 116, 1);
  static const Color COR_ERROR = Color.fromRGBO(250, 143, 143, 1);
  static const Color COR_ERROR_FORTE = Color.fromRGBO(244, 106, 106, 1);
  static const Color COR_VERMELHA = Color.fromRGBO(244, 106, 106, 1);
  static const Color COR_VERDE = Color.fromRGBO(49, 174, 116, 1);

  //Cores com opacidade
  static const Color COR_PRIMARY_OPACITY = Color.fromRGBO(155, 77, 238, 0.1);
  static const Color COR_PRIMARY_OPACITY2 = Color.fromRGBO(155, 77, 238, 0.2);
  static const Color COR_PRIMARY_OPACITY3 = Color.fromRGBO(155, 77, 238, 0.3);
  static const Color COR_PRIMARY_OPACITY4 = Color.fromRGBO(155, 77, 238, 0.4);
  static const Color COR_PRIMARY_OPACITY5 = Color.fromRGBO(155, 77, 238, 0.5);
  static const Color COR_PRIMARY_OPACITY7 = Color.fromRGBO(155, 77, 238, 0.7);


}