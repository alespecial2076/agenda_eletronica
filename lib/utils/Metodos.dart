import 'dart:math';

import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';

Future push(BuildContext context, Widget tela){
  Navigator.of(context).push(MaterialPageRoute(builder: (context) => tela));
}

Future pushReplacement(BuildContext context, Widget tela){
  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => tela));
}

Future popTela(BuildContext context){
  Navigator.of(context).pop();
}
Future openModal({@required BuildContext context, @required Widget tela, expand = true}){
  showBarModalBottomSheet(
    expand: expand,
    context: context,
    backgroundColor: Colors.transparent,
    builder: (context) => tela,
  );
}

Future openModalBaixo({@required BuildContext context, @required Widget tela, expand = false}){
  showCupertinoModalBottomSheet(
    expand: expand,
    context: context,
    backgroundColor: Colors.transparent,
    builder: (context) => tela,
  );
}