class Strings {

  static const String NOME_APP = "Agenda Eletrônica";
  static const String NOME_DEV = "Alex Anderson";
  static const String EMAIL_CONTATO_APP = "ALESPECIAL2011@GMAIL.COM";
  static const String URL_LINKEDIN = "https://www.linkedin.com/in/alex-anderson-b05368155/";
  static const String URL_WPP = "https://api.whatsapp.com/send?phone=5531989064421";

}