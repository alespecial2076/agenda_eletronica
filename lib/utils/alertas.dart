import 'dart:async';

import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/material.dart';


final _formKey = GlobalKey<FormState>();


alertaSimples(context, {titulo, msg, corTitulo = Cores.COR_ERROR}){
  Widget naoButton = FlatButton(
    child: Text(
      "Ok",
      style: TextStyle(color: Cores.COR_PRIMARY),
    ),
    onPressed: () {
      Navigator.pop(context);
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Row(
      children: [
        Icon(Icons.error_outline),
        SizedBox(
          width: 4,
        ),
        Expanded(
          child: TextApp(
            text: titulo,
            fontSize: 18,
            textColor: corTitulo,
            bold: true,
          ),
        )
      ],
    ),
    content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextApp(
          text: msg,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
      ],
    ),
    actions: [
      //simButton,
      naoButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}

alertaImagemZoom(context, {titulo = "titulo", msg = "msg", corTitulo = Cores.COR_ERROR, Widget widgetImg}){

  if(widgetImg == null){
    widgetImg = Container();
  }

  Widget naoButton = FlatButton(
    child: Text(
      "Ok",
      style: TextStyle(color: Cores.COR_PRIMARY),
    ),
    onPressed: () {
      Navigator.pop(context);
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Row(
      children: [
        Icon(Icons.error_outline),
        SizedBox(
          width: 4,
        ),
        Expanded(
          child: TextApp(
            text: titulo,
            fontSize: 18,
            textColor: corTitulo,
            bold: true,
          ),
        )
      ],
    ),
    content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextApp(
          text: msg,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
        widgetImg,
      ],
    ),
    actions: [
      //simButton,
      naoButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}

alertaDuvidasWpp(context, {titulo = "Tire suas dúvidas", msg = "Nos chame no Whatsapp", corTitulo = Cores.COR_ERROR}){
  Widget naoButton = FlatButton(
    child: Text(
      "Ok",
      style: TextStyle(color: Cores.COR_PRIMARY),
    ),
    onPressed: () {
      Navigator.pop(context);
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Row(
      children: [
        Icon(Icons.error_outline),
        SizedBox(
          width: 4,
        ),
        Expanded(
          child: TextApp(
            text: titulo,
            fontSize: 18,
            textColor: corTitulo,
            bold: true,
          ),
        )
      ],
    ),
    content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextApp(
          text: msg,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
      ],
    ),
    actions: [
      //simButton,
      naoButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}

alertaSimplesDoisBotoes(context, {titulo, msg, corTitulo = Cores.COR_ERROR, corTxtConfirmar = Cores.COR_PRIMARY, txtConfirmar = "Ok", txtCancelar = "cancelar", VoidCallback clickConfirmar, VoidCallback clickCancelar}){
  Widget confirmarButton = FlatButton(
    child: Text(
      txtConfirmar,
      style: TextStyle(color: corTxtConfirmar),
    ),
    onPressed: () {
      clickConfirmar();
      Navigator.pop(context);
    },
  );
  Widget cancelarButton = FlatButton(
    child: Text(
      txtCancelar,
      style: TextStyle(color: Colors.grey),
    ),
    onPressed: () {
      clickCancelar();
      Navigator.pop(context);
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Row(
      children: [
        Icon(Icons.error_outline),
        SizedBox(
          width: 4,
        ),
        Expanded(
          child: TextApp(
            text: titulo,
            fontSize: 18,
            textColor: corTitulo,
            bold: true,
          ),
        )
      ],
    ),
    content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextApp(
          text: msg,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
      ],
    ),
    actions: [
      cancelarButton,
      confirmarButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}

alertaSimples2(context, {@required String titulo, @required String msg, msg2 = "", msg3 = "", corTitulo = Cores.COR_ERROR}){
  Widget naoButton = FlatButton(
    child: Text(
      "Ok",
      style: TextStyle(color: Cores.COR_PRIMARY),
    ),
    onPressed: () {
      Navigator.pop(context);
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Row(
      children: [
        Icon(Icons.error_outline),
        SizedBox(
          width: 4,
        ),
        TextApp(
          text: titulo,
          fontSize: 18,
          textColor: corTitulo,
          bold: true,
        )
      ],
    ),
    content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextApp(
          text: msg,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
        SizedBox(height: 8,),
        TextApp(
          text: msg2,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          //bold: true,
        ),
        SizedBox(height: 8,),
        TextApp(
          text: msg3,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
      ],
    ),
    actions: [
      //simButton,
      naoButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}

alertaDelete(context, {titulo, msg, corTitulo = Cores.COR_ERROR, @required VoidCallback funcExcluir, txtConfirmar = "Sim, excluir!"}){
  Widget naoButton = FlatButton(
    child: Text(
      txtConfirmar,
      style: TextStyle(color: Cores.COR_ERROR_FORTE),
    ),
    onPressed: () {
      funcExcluir();
      Navigator.pop(context);
    },
  );
  Widget simButton = FlatButton(
    child: Text(
      "Não",
      style: TextStyle(color: Cores.COR_DARK_LIGTH),
    ),
    onPressed: () {
      Navigator.pop(context);
    },
  );
  // configura o  AlertDialog
  AlertDialog alerta = AlertDialog(
    title: Row(
      children: [
        Icon(Icons.error_outline),
        SizedBox(
          width: 4,
        ),
        TextApp(
          text: titulo,
          fontSize: 18,
          textColor: corTitulo,
          bold: true,
        )
      ],
    ),
    content: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        TextApp(
          text: msg,
          fontSize: 14,
          textColor: Cores.COR_DARK_LIGTH,
          bold: true,
        ),
      ],
    ),
    actions: [
      simButton,
      naoButton,
    ],
  );
  // exibe o dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alerta;
    },
  );
}

