
import 'package:http/http.dart' as http;

Future<http.Response> get(String url) async {
  final headers = await _headers();
  var response = await http.get(url,);
  return response;
}

Future<http.Response> post(String url, {body}) async {
  final headers = await _headers();
  var response = await http.post(url, body: body,);
  return response;
}

Future<http.Response> put(String url, {body}) async{
  final headers = await _headers();
  var response = await http.put(url, body: body,);
  return response;
}

Future<http.Response> delete(String url) async{
  final headers = await _headers();
  var response = await http.delete(url,);
  return response;
}

Future<Map<String, String>> _headers() async{
  /*
  User usuario =  await User.get();

  Map<String, String> headers = {
    "Content-Type": "application/json",
    "Authorization": "Bearer ${usuario.token}"
  };
   */
  //return headers;
}
