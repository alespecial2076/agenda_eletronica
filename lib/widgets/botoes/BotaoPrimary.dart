
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/material.dart';

class BotaoPrimary extends StatelessWidget {

  String text;
  VoidCallback onPressed;
  double tamanho;
  bool carregando;
  Color disabledColor;
  Color corBackground;

  BotaoPrimary(
      {
        @required this.text,
        @required this.onPressed,
        this.tamanho = 46,
        this.carregando = false,
        this.disabledColor = Cores.COR_PRIMARY_LIGTH,
        this.corBackground = Cores.COR_PRIMARY,
      });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: tamanho,
      width: double.infinity,
      child: RaisedButton(
        color: corBackground,
        child:
        !carregando
            ? TextApp(
                  text: text,
                  textColor: Colors.white,
                  fontSize: 16,
              )
            : CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
              ),
        onPressed: !carregando ? onPressed : null,
        textColor: Colors.white,
        disabledColor: disabledColor,
      ),
    );
  }
}
