import 'package:agenda_eletronica/models/contatos/Contato.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/widgets/circle/CircleAvatarCache.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CardContato extends StatelessWidget {

  Contato contato;
  CardContato({@required this.contato,});

  @override
  Widget build(BuildContext context) {


    return Card(
      child: Container(
        //margin: EdgeInsets.only(bottom: 8),
        padding: EdgeInsets.only(top: 8, bottom: 8,),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              //color: Colors.red,
              padding: EdgeInsets.only(left: 8, right: 8),
              child:
              contato.fotoUint8 != null
                  ? CircleAvatar(
                backgroundImage:  MemoryImage(contato.fotoUint8),
                backgroundColor: Colors.blueGrey[50],
                radius: 24,
              ) :
              CircleAvatarCache(
                urlImagem: contato.urlFoto,
                size: 48,
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextApp(
                    text: "${contato.nome}",
                    textColor: Cores.COR_DARK,
                    bold: true,
                    fontSize: 13,
                    fontFamily: "OpenSans",
                  ),
                  SizedBox(height: 2,),
                  TextApp(
                    text: "${contato.telefoneCelular}",
                    textColor: Cores.COR_DARK,
                    bold: false,
                    fontFamily: "OpenSans",
                  ),
                  SizedBox(height: 2,),
                  contato.email != null && contato.email.length > 1 ?
                  TextApp(
                    text: "${contato.email}",
                    textColor: Cores.COR_DARK,
                    bold: false,
                    fontFamily: "OpenSans",
                  ) : Container(),
                  SizedBox(height: 2,),
                ],
              ),
            ),

            GestureDetector(
              onTap: (){
                String numberFormatado =  contato.telefoneCelular.replaceAll(new RegExp(r'[^0-9]'),''); // '23'
                var url = "https://api.whatsapp.com/send?phone=55$numberFormatado.";
                _launchURL(url);
              },
              child: Container(
                margin: EdgeInsets.only(right: 8),
                child: Image.asset("assets/imagens/iconWpp.png", height: 30,),
              ),
            )
          ],
        ),
      ),
    );
  }

  _launchURL(url) async => await canLaunch(url)
      ? await launch(url) : throw 'Not found $url';
}
