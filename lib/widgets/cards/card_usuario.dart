
import 'package:agenda_eletronica/models/users/User.dart';
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:agenda_eletronica/widgets/circle/CircleAvatarCache.dart';
import 'package:agenda_eletronica/widgets/texts/TextApp.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CardUsuario extends StatelessWidget {

  User usuario;
  CardUsuario({@required this.usuario,});

  @override
  Widget build(BuildContext context) {


    return Card(
      child: Container(
        //margin: EdgeInsets.only(bottom: 8),
        padding: EdgeInsets.only(top: 8, bottom: 8,),
        child: Row(
          children: [
            Container(
              //color: Colors.red,
              padding: EdgeInsets.only(left: 8, right: 8),
              child:
              usuario.fotoUint8 != null
                  ? CircleAvatar(
                backgroundImage:  MemoryImage(usuario.fotoUint8),
                backgroundColor: Colors.blueGrey[50],
                radius: 24,
              ) :
              CircleAvatarCache(
                urlImagem: usuario.urlFoto,
                size: 48,
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  TextApp(
                    text: "${usuario.nome}",
                    textColor: Cores.COR_DARK,
                    bold: true,
                    fontSize: 13,
                    fontFamily: "OpenSans",
                  ),
                  SizedBox(height: 2,),
                  TextApp(
                    text: "${usuario.telefone}",
                    textColor: Cores.COR_DARK,
                    bold: false,
                    fontFamily: "OpenSans",
                  ),
                  SizedBox(height: 2,),
                  TextApp(
                    text: "${usuario.email}",
                    textColor: Cores.COR_DARK,
                    bold: false,
                    fontFamily: "OpenSans",
                  ),
                  SizedBox(height: 2,),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}
