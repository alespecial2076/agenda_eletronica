
import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:flutter/material.dart';

class TextApp extends StatelessWidget {

  String text;
  Color textColor;
  double fontSize;
  String fontFamily;
  bool bold;
  bool riscoMeio;
  bool centralizar;
  int maxlines;

  TextApp({@required this.text, this.textColor = Cores.COR_PRIMARY, this.fontSize = 12, this.fontFamily = "Roboto", this.bold = false, this.riscoMeio = false, this.maxlines = 200, this.centralizar = false});

  //Tamanho fontes: 12, 18, 24, 36, 48, 60, 72

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxlines,
      overflow: TextOverflow.ellipsis,
      textAlign: centralizar ? TextAlign.center : TextAlign.start,
      style: TextStyle(
        fontFamily: fontFamily,
        fontSize: fontSize,
        fontWeight: bold ? FontWeight.bold : FontWeight.normal,
        color: textColor,
        decoration: riscoMeio ? TextDecoration.lineThrough : TextDecoration.none,
      ),
    );
  }
}
