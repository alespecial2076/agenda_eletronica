import 'package:flutter/material.dart';

class TextError extends StatelessWidget {

  String msg;
  VoidCallback onTap;
  TextError(this.msg, {this.onTap});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        InkWell(
          onTap: onTap,
          child: Column(
            children: [
              Text(
                msg,
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 18
                ),
              ),
              Text("Tentar Novamente", style: TextStyle(fontSize: 18, decoration: TextDecoration.underline, color: Colors.blue), )
            ],
          ),
        )
      ],
    );
  }
}
