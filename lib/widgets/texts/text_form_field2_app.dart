import 'package:flutter/material.dart';

class TextFormField2App extends StatelessWidget {

  String labelText;
  String hintText;
  bool obscureText;
  TextEditingController controller;
  FormFieldValidator<String> validator;
  TextInputType keyboardType;
  TextInputAction textInputAction;
  FocusNode focusNode;
  FocusNode nextFocus;
  double fontSizeHint;
  double fontSizeLabel;
  Color corTextHint;
  Color corTextLabel;

  TextFormField2App({
    this.labelText,
    this.hintText,
    this.obscureText = false,
    this.controller,
    this.validator,
    this.keyboardType,
    this.textInputAction,
    this.focusNode,
    this.nextFocus,
    this.fontSizeHint = 18,
    this.fontSizeLabel = 16,
    this.corTextHint = Colors.green,
    this.corTextLabel = Colors.blue,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: 56,
      child: TextFormField(
        controller: controller,
        keyboardType: TextInputType.text,
        validator: validator,
        style: TextStyle(
          color: Colors.blue,
          fontSize: 20,
        ),
        decoration: new InputDecoration(
          hintText: hintText,
          labelText: labelText,
        ),
      ),
    );
  }
}
