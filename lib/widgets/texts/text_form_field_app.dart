import 'package:agenda_eletronica/utils/Cores.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormFieldApp extends StatelessWidget {

  String labelText;
  String hintText;
  bool obscureText;
  double fontSize;
  double hintSize;
  double labelSize;
  TextEditingController controller;
  FormFieldValidator<String> validator;
  TextInputType keyboardType;
  TextInputAction textInputAction;
  List<TextInputFormatter> inputFormatters;
  FocusNode focusNode;
  FocusNode nextFocus;
  int maxLength;
  double width;
  TextAlign textAlign;
  Function onChanged;
  String prefixText;
  bool habilitado;
  int maxLines;

  TextFormFieldApp({
    this.labelText,
    this.hintText,
    this.fontSize = 16,
    this.hintSize = 16,
    this.labelSize = 16,
    this.obscureText = false,
    this.controller,
    this.validator,
    this.keyboardType,
    this.textInputAction,
    this.focusNode,
    this.nextFocus,
    this.inputFormatters,
    this.maxLength,
    this.width,
    this.textAlign = TextAlign.start,
    this.onChanged,
    this.prefixText = "",
    this.habilitado = true,
    this.maxLines = 1
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: 48,
      //width: width != null ? width : 120,
      child: TextFormField(
        enabled: habilitado,
        maxLength: maxLength,
        validator: validator,
        controller: controller,
        keyboardType: keyboardType,
        keyboardAppearance: Brightness.light,
        textInputAction: textInputAction,
        focusNode: focusNode,
        inputFormatters: inputFormatters,
        textAlign: textAlign,
        maxLines: maxLines,
        onFieldSubmitted: (String text) {
          if(nextFocus != null) {
            FocusScope.of(context).requestFocus(nextFocus);
          }
          print("click teclado $text");
        },
        onChanged: onChanged,
        style: TextStyle(
          fontSize: fontSize,
          color: habilitado ? Cores.COR_DARK_LIGTH : Colors.grey[400],
        ),
        obscureText: obscureText,
        decoration:
        InputDecoration(
            contentPadding: new EdgeInsets.symmetric(vertical: 12.0, horizontal: 10.0),
            enabledBorder: const OutlineInputBorder(
              // Cor e largura borda (desabilitada)
              borderSide: const BorderSide(color: Cores.COR_DARK, width: 0.0),
            ),
          focusedBorder: const OutlineInputBorder(
            // Cor e largura borda (Habilitada)
            borderSide: const BorderSide(color: Cores.COR_PRIMARY, width: 2.0),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4),
          ),
            labelText: labelText,
            //counterText: labelText,
            prefixText: prefixText,
            hintText: hintText,
            hintStyle: TextStyle(
                color: Colors.grey,
                fontSize: hintSize
            ),
            labelStyle: TextStyle(
              fontSize: labelSize,
              color: Colors.grey,
            )
        ),
      ),
    );
  }
}
